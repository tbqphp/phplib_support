<?php

define('BASE_DIR', __DIR__);

ini_set("display_errors", "On");

require_once BASE_DIR . '/vendor/autoload.php';

use Chentu\Support\Log;
use Chentu\Support\Redis;

Chentu\Support\Helper::appInit();

Log::info('log test');
Log::channel('log2')->info('log test');
