<?php

declare(strict_types=1);

define('BASE_DIR', dirname(__DIR__));
require_once BASE_DIR . '/vendor/autoload.php';
Chentu\Support\Helper::appInit();

use Chentu\HttpRequest;
use PHPUnit\Framework\TestCase;

final class GuzzleTest extends TestCase
{
    public function test_get(): void
    {
        $url = 'http://baishanyunpiao.com/prod-api/open/api/token';
        $data = [
            'accessKey' => 'aaaaaaaaa',
            'secretKey' => 'bbbbbbbbbb',
        ];
        $rt = HttpRequest::postJson($url, $data);
        $this->assertTrue($rt['state']);
    }
}
