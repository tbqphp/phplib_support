<?php

declare(strict_types=1);
define('BASE_DIR', dirname(__DIR__));
require_once BASE_DIR . '/vendor/autoload.php';
Chentu\Support\Helper::appInit();

use PHPUnit\Framework\TestCase;

final class HelperTest extends TestCase
{
    public function test_get(): void
    {
        //$this->assertEquals(6, 7);
        $this->assertEquals(env('DB_NAME'), 'laravel');
    }
}
