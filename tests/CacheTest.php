<?php
declare(strict_types=1);

define('BASE_DIR', dirname(__DIR__));
require_once BASE_DIR . '/vendor/autoload.php';
Chentu\Support\Helper::appInit();

use Chentu\Cache;
use Monolog\Test\TestCase;

final class CacheTest extends TestCase
{
    public function test_get(): void
    {
        $key = __METHOD__ . '|test1';
        $this->assertTrue(Cache::save($key, 'testValue', 2));

        $v = Cache::fetch($key);
        $this->assertEquals($v, 'testValue');

        sleep(3);
        $v = Cache::fetch($key);
        $this->assertNotEquals($v, 'testValue');
    }
}
