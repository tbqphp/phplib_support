<?php

/**
 * https://packagist.org/packages/hashids/hashids
 */

define('BASE_DIR', __DIR__);

ini_set("display_errors", "On");

require_once BASE_DIR . '/vendor/autoload.php';

use Hashids\Hashids;

$hashids = new Hashids();

$id = $hashids->encode(10);
var_dump($id);

$id = $hashids->decode($id);
var_dump($id);
