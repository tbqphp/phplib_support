<?php

return [
    'default' => [
        'database' => env('DB_NAME'),
        'driver' => 'mysql',
        'read' => [
            'host' => env('DB_HOST', '127.0.0.1'),
        ],
        'write' => [
            'host' => env('DB_HOST_READ', '127.0.0.1'),
        ],
        'username' => env('DB_USER', 'root'),
        'password' => env('DB_PASS', '111111'),
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix' => '',
        'port' => env('DB_PORT', '33065'),
    ],
    'data' => [
        'database' => env('DB_NAME_DATA'),
        'driver' => 'mysql',
        'read' => [
            'host' => env('DB_HOST', '127.0.0.1'),
        ],
        'write' => [
            'host' => env('DB_HOST_READ', '127.0.0.1'),
        ],
        'username' => env('DB_USER', 'root'),
        'password' => env('DB_PASS', '111111'),
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix' => '',
        'port' => env('DB_PORT', '33065'),
    ],
];
