<?php

/**
 * https://www.doctrine-project.org/projects/doctrine-cache/en/current/index.html
 *  composer require symfony/cache
 *    composer require doctrine/cache
 */

namespace Chentu;

use Doctrine\Common\Cache\Psr6\DoctrineProvider;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

class Cache
{
    private static $instance;
    private static function getInstance()
    {
        if (!self::$instance) {
            $cachePool = new FilesystemAdapter();
            self::$instance = DoctrineProvider::wrap($cachePool);
        }
        return self::$instance;
    }
    /**
     * @param $key
     *
     */
    public static function save(string $key, $data, $lifetime = 0)
    {
        return self::getInstance()->save($key, $data, $lifetime);
    }
    /**
     * @param $key
     *
     */
    public static function fetch(string $key)
    {
        return self::getInstance()->fetch($key);
    }
    /**
     * @param $key
     *
     */
    public static function delete(string $key)
    {
        return self::getInstance()->delete($key);
    }
}
