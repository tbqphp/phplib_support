<?php

namespace Chentu\Support;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;

class Db extends Capsule
{
    public static function dbInit()
    {
        $capsule = new Db();

        $conns = self::getConns();
        foreach ($conns as $name => $conn) {
            $capsule->addConnection($conn, $name);
        }

        $capsule->setEventDispatcher(new Dispatcher(new Container()));

        // Make this Capsule instance available globally via static methods... (optional)
        $capsule->setAsGlobal();

        // Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
        $capsule->bootEloquent();
    }

    private static function getConns()
    {
        $conf = include(BASE_DIR . '/config/db.php');
        return $conf;
    }
}
