<?php

namespace Chentu\Support;

class Helper
{
    public static function appInit()
    {
        // env()函数生效
        $dotenv = \Dotenv\Dotenv::createImmutable(BASE_DIR);
        $dotenv->load();

        Db::dbInit();
    }
}
