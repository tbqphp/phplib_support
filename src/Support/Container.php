<?php
/**
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the MIT-LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace Chentu\Support;

use Chentu\Config;

/**
 * Class Container
 * @package support
 * @method static mixed get($name)
 * @method static mixed make($name, array $parameters)
 * @method static bool has($name)
 */
class Container
{
    /**
     * Instance
     * @param string $plugin
     * @return array|mixed|void|null
     */
    public static function instance()
    {
        $builder = new \DI\ContainerBuilder();
        $builder->addDefinitions([
            //\Workerman\MySQL\Connection::class => \DI\factory(function (){
                //return new \Workerman\MySQL\Connection($this->conf['db.host'], $this->conf->get('db.port'), $this->conf->get('db.user'), $this->conf['db.pass'], $this->conf['db.name']);
            //}),
        ]);
        return $builder->build();
    }

    /**
     * @param string $name
     * @param array $arguments
     * @return mixed
     */
    public static function __callStatic(string $name, array $arguments)
    {
        return static::instance()->{$name}(... $arguments);
    }
}
