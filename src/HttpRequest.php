<?php

namespace Chentu;

use GuzzleHttp\Client;
use Exception;

class HttpRequest
{
    public static function postJson($url, $data)
    {
        $client = new Client(['base_uri' => $url]);
        $response = $client->post($url, [
            'json' => $data,
        ]);
        $code = $response->getStatusCode();
        if ($code != 200) {
            throw new Exception('请求失败');
        }
        $body = (string) $response->getBody();
        $body = json_decode($body, true);
        if (!is_array($body)) {
            throw new Exception('请求返回错误');
        }
        return $body;
    }
}
