<?php

use Bunny\Channel;
use Workerman\RabbitMQ\Client;
use Bunny\Message;

function getBCDTime($inputData)
{
    $time_year = '20' . substr($inputData, 0, 2);
    $time_month = substr($inputData, 2, 2);
    $time_date = substr($inputData, 4, 2);
    $time_hour = substr($inputData, 6, 2);
    $time_min = substr($inputData, 8, 2);
    $time_second = substr($inputData, 10, 2);
    return "{$time_year}-{$time_month}-{$time_date} {$time_hour}:{$time_min}:{$time_second}";
}

/**
 * 和校验
 * @param [type] $s [description]
 */
function HeJiaoYan($s)
{
    $s = bin2hex($s);
    $jiaoyan = 0;
    for ($n = 0; $n < strlen($s); $n++) {
        $jiaoyan = hexdec(substr($s, $n * 2, 2)) + $jiaoyan;
    }
    $jiaoyan = $jiaoyan % 256;
    $jiaoyanma = dechex($jiaoyan);
    if ($jiaoyan < 16) {
        $jiaoyanma = '0' . $jiaoyanma;
    }

    return $jiaoyanma;
}

function HeJiaoYanForHex($s)
{
    $jiaoyan = 0;
    for ($n = 0; $n < strlen($s); $n++) {
        $jiaoyan = hexdec(substr($s, $n * 2, 2)) + $jiaoyan;
    }
    $jiaoyan = $jiaoyan % 256;
    $jiaoyanma = dechex($jiaoyan);
    if ($jiaoyan < 16) {
        $jiaoyanma = '0' . $jiaoyanma;
    }

    return $jiaoyanma;
}

/**
 * @param $devicePrinthead
 * @return string|void
 *
 * 设备名称的16进制表示
 */
function str2hex($str, $bytes, $iconv = true)
{
    if (empty($str)) {
        return str_repeat('00', $bytes);
    }
    if ($iconv) {
        $str = iconv("utf-8", "gb2312//IGNORE", $str);
    }
    $str = bin2hex($str);
    $len = strlen($str);
    if ($len <= ($bytes - 1) * 2) {
        $str = $str . str_replace('0', $bytes * 2 - $len);
    } else {
        $str = substr($str, 0, $bytes * 2) . '00';
    }
    return $str;
}

function hex2float($hexValue, $decision = 2)
{
    $v = hexdec($hexValue);
    $x = ($v & ((1 << 23) - 1)) + (1 << 23) * ($v >> 31 | 1);
    $exp = ($v >> 23 & 0xFF) - 127;
    $v = $x * pow(2, $exp - 23);
    return sprintf('%.' . $decision * 2 . 'f', $v);
}

//数组转对象
function arrayToObject($e)
{
    if (gettype($e) != 'array') {
        return;
    }

    foreach ($e as $k => $v) {
        if (gettype($v) == 'array' || getType($v) == 'object') {
            $e[$k] = (object)arrayToObject($v);
        }
    }
    return (object)$e;
}

function sendMQ($params, $queue, $msg)
{
    (new Client($params))->connect()->then(function (Client $client) {
        return $client->channel();
    })->then(function (Channel $channel) use ($queue) {
        return $channel->queueDeclare($queue, false, false, false, false)->then(function () use ($channel) {
            return $channel;
        });
    })->then(function (Channel $channel) use ($queue, $msg) {
        return $channel->publish($msg, [], '', $queue)->then(function () use ($channel) {
            return $channel;
        });
    })->then(function (Channel $channel) {
        $client = $channel->getClient();
        return $channel->close()->then(function () use ($client) {
            return $client;
        });
    })->then(function (Client $client) {
        $client->disconnect();
    });
}

function receiveMQ($params, $queue, $callback)
{
    (new Client($params))->connect()->then(function (Client $client) {
        return $client->channel();
    })->then(function (Channel $channel) use ($queue) {
        return $channel->queueDeclare($queue, false, false, false, false)->then(function () use ($channel) {
            return $channel;
        });
    })->then(function (Channel $channel) use ($queue, $callback) {
        echo ' [*] Waiting for messages. To exit press CTRL+C', "\n";
        $channel->consume(
            $callback,
            $queue,
            '',
            false,
            true
        );
    });
}

/**
 * 按指定字节数取数字的16进制字符串, 并左侧补0
 * $bytes 字节数
 */
function dechexBytes($number, $bytes)
{
    $ff = str_repeat('ff', $bytes);
    $rt = dechex(intval($number) & hexdec($ff)); //过滤超过指定字节的部分
    return str_pad($rt, 2 * $bytes, '0', STR_PAD_LEFT);
}

/**
 * @param $str
 * @param $bytes
 * @param $unsigned 是否为无符号数
 * @return float|int
 */
function hexdecBytes($str, $bytes, $unsigned = false)
{
    $value = hexdec($str);
    if (!$unsigned) {
        $ff = str_repeat('ff', $bytes);
        $tmp = hexdec($ff) + 1;// 字节最大值加1
        if ($value >= intval($tmp / 2)) {
            $value = $value - $tmp;
        }
    }
    return $value;
}
function utf8Strlen ($string = null, $chinese = 2, $other = 1) {
    // 将字符串分解为单元
    preg_match_all("/./us", $string, $match);
    $pattern = '/[^\x00-\x80]/';
    $count = 0;
    foreach ($match[0] as $value) {
        $count = preg_match($pattern, $value) ? ($count + $chinese) : ($count + $other);
    }

    return $count;
}
/** * 验证输入的手机号码
 * @access  public
 * @param string $user_mobile 需要验证的手机号码
 * @return bool
 */
function is_mobile ($user_mobile) {
    $chars = "/^((\(\d{2,3}\))|(\d{3}\-))?1(3|5|8|9|7|1|2|4|6|0)\d{9}$/";
    if (preg_match($chars, $user_mobile)) {
        return true;
    } else {
        return false;
    }
}
function object_to_array($obj){
    $_arr=is_object($obj)?get_object_vars($obj):$obj;
    $arr = null;
    foreach($_arr as $key=>$val){
        $val=(is_array($val))||is_object($val)?object_to_array($val):$val;
        $arr[$key]=$val;
    }
    return $arr;
}
//数字转中文
function numToWord($num){
    $chiNum = array('零', '一', '二', '三', '四', '五', '六', '七', '八', '九');
    $chiUni = array('','十', '百', '千', '万', '十', '百', '千', '亿');
    $chiStr = '';
    $num_str = (string)$num;
    $count = strlen($num_str);
    $last_flag = true; //上一个 是否为0
    $zero_flag = true; //是否第一个
    $temp_num = null; //临时数字
    $chiStr = '';//拼接结果
    if ($count == 2) {//两位数
        $temp_num = $num_str[0];
        $chiStr = $temp_num == 1 ? $chiUni[1] : $chiNum[$temp_num].$chiUni[1];
        $temp_num = $num_str[1];
        $chiStr .= $temp_num == 0 ? '' : $chiNum[$temp_num];
    }else if($count > 2){
        $index = 0;
        for ($i=$count-1; $i >= 0 ; $i--) {
            $temp_num = $num_str[$i];
            if ($temp_num == 0) {
                if (!$zero_flag && !$last_flag ) {
                    $chiStr = $chiNum[$temp_num]. $chiStr;
                    $last_flag = true;
                }

                if($index == 4 && $temp_num == 0){
                    $chiStr = "万".$chiStr;
                }
            }else{
                if($i == 0 && $temp_num == 1 && $index == 1 && $index == 5){
                    $chiStr = $chiUni[$index%9] .$chiStr;
                }else{
                    $chiStr = $chiNum[$temp_num].$chiUni[$index%9] .$chiStr;
                }
                $zero_flag = false;
                $last_flag = false;
            }
            $index ++;
        }
    }else{
        $chiStr = $chiNum[$num_str[0]];
    }
    return $chiStr;
}

//中文转数字
function cn2num($string){
    if(is_numeric($string)){
        return $string;
    }
    // '仟' => '千','佰' => '百','拾' => '十',
    $string = str_replace('仟', '千', $string);
    $string = str_replace('佰', '百', $string);
    $string = str_replace('拾', '十', $string);
    $num = 0;
    $wan = explode('万', $string);
    if (count($wan) > 1) {
        $num += cn2num($wan[0]) * 10000;
        $string = $wan[1];
    }
    $qian = explode('千', $string);
    if (count($qian) > 1) {
        $num += cn2num($qian[0]) * 1000;
        $string = $qian[1];
    }
    $bai = explode('百', $string);
    if (count($bai) > 1) {
        $num += cn2num($bai[0]) * 100;
        $string = $bai[1];
    }
    $shi = explode('十', $string);
    if (count($shi) > 1) {
        $num += cn2num($shi[0] ? $shi[0] : '一') * 10;
        $string = $shi[1] ? $shi[1] : '零';
    }
    $ling = explode('零', $string);
    if (count($ling) > 1) {
        $string = $ling[1];
    }
    $d = array(
        '一' => '1','二' => '2','三' => '3','四' => '4','五' => '5','六' => '6','七' => '7','八' => '8','九' => '9',
        '壹' => '1','贰' => '2','叁' => '3','肆' => '4','伍' => '5','陆' => '6','柒' => '7','捌' => '8','玖' => '9',
        '零' => 0, '0' => 0, 'O' => 0, 'o' => 0,
        '两' => 2
    );
    return $num + @$d[$string];
}
function checkDatetime($str, $format = "Y-m-d H:i:s"){
    $time = strtotime($str);  //转换为时间戳
    $checkstr = date($format, $time); //在转换为时间格式
    if($str == $checkstr){
        return true;
    }else{
        return false;
    }
}
