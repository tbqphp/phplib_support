<?php

namespace Chentu\ContainerTest;

class B
{

    private $a;

    public function __construct(A $a)
    {
        $this->a = $a;
    }

    public function funB()
    {
        var_dump($this->a->funA());
    }
}
